clear all
close all
clc


%optimise code for best result in (1/8) and other 

for howmany = 1:7
    for which = 1:length(combnk(1:8,howmany))
    col = combnk(1:8,howmany);
    data = importdata('abalone.data');
    [size_x,size_y] = size(data);
    A = zeros(size_x, howmany+1);
    A(:,1) = ones(size_x, 1);
    A(:,2:1+howmany) = data(:,col(which,:));
    %Y = zeros(size_x, 1)
    Y = data(:, end);
    theta = A\Y;%inv(A'*A)*A'*Y;
    error =  A * theta - Y;
    mse(end+1) = (error'*error)/length(error);
    end
end



%% this is for 8 of them
data = importdata('abalone.data');
[size_x,size_y] = size(data);
A = zeros(size_x, size_y);
A(:,1) = ones(size_x, 1);
A(:,2:end) = data(:,1:size_y - 1);
%Y = zeros(size_x, 1)
Y = data(:, end);
theta = inv(A'*A)*A'*Y;
error =  A * theta - Y;
mse(end+1) = (error'*error)/length(error);


min(mse)


%% this for gradient decent
data = importdata('abalone.data');
[size_x,size_y] = size(data);
A = zeros(size_x, size_y);
A(:,1) = ones(size_x, 1);
A(:,2:end) = data(:,1:size_y - 1);
Y = data(:, end);
theta = zeros(size(A,2), 1);
for iterations = 1:1000
    for i = 1:size(A, 1)
        for j = 1:size(theta,1)
            theta(j) = theta(j) - 0.0001 * (A(i,:) * theta -Y(i)) * A(i,j);
        end
    end
end
error =  A * theta - Y;
mse(end+1) = (error'*error)/length(error)


